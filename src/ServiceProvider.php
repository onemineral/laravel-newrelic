<?php

namespace Onemineral\Newrelic;

use Illuminate\Queue\Events\JobProcessing;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;

class ServiceProvider extends IlluminateServiceProvider
{

    /**
     *
     */
    public function register()
    {
        if($this->isEnabled()) {
            newrelic_set_appname($this->getName());
        }
    }

    /**
     *
     */
    public function boot()
    {
        if($this->isEnabled()) {
            Queue::before(function (JobProcessing $event) {
                newrelic_start_transaction($this->getName());
                newrelic_name_transaction($event->job->getName());
            });

            Queue::after(function () {
                $memoryUsed = round(memory_get_usage(false) / (1024 * 1024));
                newrelic_add_custom_parameter("php_memory_usage", $memoryUsed);
                newrelic_end_transaction();
            });

            App::terminating(function () {
                $memoryUsed = round(memory_get_usage(false) / (1024 * 1024));
                newrelic_add_custom_parameter("php_memory_usage", $memoryUsed);
            });
        }
    }

    /**
     * @return string
     */
    protected function getName(): string
    {
        $name = '[' . config('app.env') . '] ' . config('app.name');

        if ($this->app->runningInConsole()) {
            $name = '[Artisan] ' . $name;
        }
        return $name;
    }

    /**
     * @return bool
     */
    protected function isEnabled(): bool
    {
        return extension_loaded('newrelic');
    }

}
